<?php

require_once __DIR__ . '/autoload.php';

$idx = (int)$_GET['id'];

try {

    if (null === $idx || empty($idx) || 0 === $idx) {
        throw new Exception('There is no index in GET params');
    }

    $article = \App\Models\Article::findById($idx);

    include __DIR__ . '/templates/article.php';

} catch (Exception $e) {
    echo $e->getMessage();
}
