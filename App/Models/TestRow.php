<?php


namespace App\Models;


use App\Db;

class TestRow extends Model
{
    public const TABLE = 'test';
    public string $name;
    public string $content;


    static function addNew(string $name, string $content)
    {
        $dbh = new Db();
        $sql = 'INSERT INTO ' . self::TABLE .
            ' (name, content) 
            VALUES 
            (:name, :content);';

        return $dbh->execute(
            $sql,
            [':name' => $name , ':content' => $content]
        );
    }

    static function update(int $id, string $name, string $content):bool
    {
        $dbh = new Db();
        $sql = 'UPDATE ' . self::TABLE . ' SET name=:name, content=:content WHERE id=:id';
        return $dbh->execute(
            $sql,
            [':id' => $id, ':name' => $name, ':content' => $content]
        );
    }
}