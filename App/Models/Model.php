<?php
namespace App\Models;

use App\Db;

abstract class Model
{
    public const TABLE = '';
    public int $id;

    static function findAll(): ?array
    {
        try {

            $dbh = new Db();
            $sql = 'SELECT * FROM ' . static::TABLE;
            return $dbh->query(
                $sql, static::class
            );

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        return [];
    }

    /**
     * @param int $id
     * @return false|mixed
     */
    static function findById(int $id)
    {
        try {

            $dbh = new Db();
            $sql = 'SELECT * FROM ' . static::TABLE . ' WHERE id=:id';
            $res = $dbh->query($sql, static::class, [':id' => $id])[0];
            if (null === $res || empty($res)){
                return false;
            }
            return $res;

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        return false;
    }
}