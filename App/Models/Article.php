<?php


namespace App\Models;


use App\Db;

class Article extends Model
{
    const TABLE = 'news';
    public string $title;
    public string $content;

    static function lastThree()
    {
        $dbh = new Db();

        $sql = 'SELECT * FROM (SELECT * FROM ' .
                self::TABLE . ' ORDER BY id DESC LIMIT 3) 
                AS T ORDER BY id ASC';
//        var_dump($sql); die;
        return $dbh->query($sql, self::class);
    }
}